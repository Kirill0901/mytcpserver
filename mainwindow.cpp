#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    newServer_ = new MyTcpServer();
    newServer_->listen(QHostAddress::Any, 1234);
    connect(newServer_, SIGNAL(debugOut()), this, SLOT(debugOut()));
}

void MainWindow::debugOut()
{
    ui->logsOut->append(newServer_->getDebugString());
}

MainWindow::~MainWindow()
{
    delete newServer_;
    delete ui;
}
