#include "mytcpsocket.h"

MyTcpSocket::MyTcpSocket(QObject *parent, qintptr _socketDescriptor, qintptr _socketId):
    QTcpSocket(parent)
{
    this->setSocketDescriptor(_socketDescriptor);
    socketId_ = _socketId;
    connect(this, SIGNAL(readyRead()), this, SLOT(readSlot()));
    connect(this, SIGNAL(disconnected()), this, SLOT(disconnectedSlot()));
}

void MyTcpSocket::setSocketId(qintptr _socketId)
{
    socketId_ = _socketId;
}

qintptr MyTcpSocket::socketId()
{
    return socketId_;
}

void MyTcpSocket::readSlot()
{
    emit myReadyRead(socketId_);
}

void MyTcpSocket::disconnectedSlot()
{
    emit myDisconnected(socketId_);
}
