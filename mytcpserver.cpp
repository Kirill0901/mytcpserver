#include "mytcpserver.h"

MyTcpServer::MyTcpServer(QObject *parent):
    QTcpServer(parent)
{
    logsString_ = new QString("");
    logsStream_ = new QTextStream(logsString_);
}

void MyTcpServer::postHtml(qintptr socketId, QString htmlName)
{
    QFile page(htmlName);
    QTextStream out(allSockets_[socketId]);
    out.setCodec("UTF-8");
    if (!page.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        *(logsStream_) << "!page \"" << htmlName << "\" isn't opened!\n";
        out << "HTTP/1.1 404 Not Found\r\n"
               "Content-Type: text/html; charset=\"utf-8\"\r\n"
               "Connection: close"
               "Refresh: 120\r\n"
               "\r\n"
               "<h1>The page can not be opened<h1>" << "\r\n";
        emit debugOut();
        return;

    }
    QTextStream in(&page);
    in.setCodec("UTF-8");
    out << "HTTP/1.1 200 Ok\r\n"
           "Content-Type: text/html; charset=\"utf-8\"\r\n"
           "Connection: close"
           "Refresh: 120\r\n"
           "\r\n";
    while(!in.atEnd())
    {
        out << in.readLine() << "\r\n";
    }
    page.close();
}

void MyTcpServer::getRequest(qintptr socketId, QString *quest)
{
    QStringList list1 = quest->split(" ");
    QString htmlName = list1.at(1);
    htmlName.remove(0, 1);
    list1 = htmlName.split("?");
    htmlName = list1.at(0);
    if (htmlName == "")
    {
        postHtml(socketId, "home.html");
    }
    else
    if (!htmlName.contains("."))
    {
        postHtml(socketId, htmlName + ".html");
    }
}

void MyTcpServer::postRequest(qintptr socketId, QString *quest)
{

}

void MyTcpServer::incomingConnection(qintptr socketDescriptor)
{
    MyTcpSocket *newSocket = new MyTcpSocket(this , socketDescriptor, socketDescriptor);
    allSockets_[socketDescriptor] = newSocket;
    connect(newSocket, SIGNAL(myReadyRead(qintptr)), this, SLOT(readSocket(qintptr)));
    connect(newSocket, SIGNAL(myDisconnected(qintptr)), this, SLOT(socketDisconnected(qintptr)));
    *(logsStream_) << "new socket: " << newSocket->socketId() << "\n";
    emit debugOut();
}

void MyTcpServer::readSocket(qintptr socketId)
{
    QByteArray buffer;
    MyTcpSocket *socket = allSockets_[socketId];
    buffer = socket->readAll();
    while(!buffer.isEmpty())
    {
        QString data = QString::fromUtf8(buffer);
        *(logsStream_) << data << "\n";
        buffer = socket->readAll();
    }
    if (logsString_->contains("GET "))
    {
        getRequest(socketId, logsString_);
    }
    if (logsString_->contains("POST /test "))
    {
        postHtml(socketId, "test.html");
    }
    allSockets_[socketId]->close();
    emit debugOut();
}

void MyTcpServer::socketDisconnected(qintptr socketId)
{
    if (allSockets_.remove(socketId))
    {
        *(logsStream_) << "Socket " << socketId << " have been disconnected!\n";
    }
    else
    {
        *(logsStream_) << allSockets_.size() << " Socket isn't found in map!\n";
    }
    emit debugOut();
}

QString MyTcpServer::getDebugString()
{
    QString s = *logsString_;
    *logsString_ = "";
    return s;
}
