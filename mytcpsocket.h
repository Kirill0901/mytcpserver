#ifndef MYTCPSOCKET_H
#define MYTCPSOCKET_H

#include <QObject>
#include <QTcpSocket>

class MyTcpSocket : public QTcpSocket
{
    Q_OBJECT
public:
    explicit MyTcpSocket(QObject *parent, qintptr _socketDescriptor, qintptr _socketId);
    void setSocketId(qintptr _socketId);
    qintptr socketId();

private slots:
    void readSlot();
    void disconnectedSlot();

signals:
    void myReadyRead(qintptr);
    void myDisconnected(qintptr);

private:
    qintptr socketId_;

};

#endif // MYTCPSOCKET_H
