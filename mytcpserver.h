#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QMap>
#include <QFile>
#include <QTextEdit>
#include <QDateTime>
#include <QRegularExpression>
#include "mytcpsocket.h"

class MyTcpServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit MyTcpServer(QObject *parent = nullptr);
    QString getDebugString();

private:
    void incomingConnection(qintptr socketDescriptor);
    void postHtml(qintptr socketId, QString htmlName);
    void getRequest(qintptr socketId, QString *quest);
    void postRequest(qintptr socketId, QString *quest);

private slots:
    void readSocket(qintptr socketId);
    void socketDisconnected(qintptr socketId);

signals:
    void debugOut();

private:
    QMap<qintptr, MyTcpSocket*> allSockets_;
    QTextEdit *logsOut_;
    QTextStream *logsStream_;
    QString *logsString_;
};

#endif // MYTCPSERVER_H
