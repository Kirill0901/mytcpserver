#-------------------------------------------------
#
# Project created by QtCreator 2021-08-05T10:28:38
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyTcpServer
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    mytcpserver.cpp \
    mytcpsocket.cpp

HEADERS  += mainwindow.h \
    mytcpserver.h \
    mytcpsocket.h

FORMS    += mainwindow.ui
